/*13. Голодные игры. Родительский процесс создает указанное
количество дочерних.Каждый дочерний процесс удаляет идентификатор. 
Игра прекращается. когда останется один участник, родительский процесс 
сообщает идентификатор победителя.*/

#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <sys/ipc.h>
#include <sys/shm.h>
#include <sys/sem.h>
#include <ctype.h>
#include <string.h>
#define MAX_LEN 1024

union semun {
    int val; // значение для SETVAL
    struct semid_ds *buf; // буферы для IPC_STAT, IPC_SET
    unsigned short *array; // массивы для GETALL, SETALL
    // часть, особенная для Linux:
    struct seminfo *__buf; // буфер для IPC_INFO
};
void HungerGames(char *number) {
    int space = atoi(number);
    int semid, shmid, status = 0,j = space;
    pid_t wpid, pid[space];
    key_t key = IPC_PRIVATE;
    double *shm;
    union semun arg;
    union semun arg2;
    struct sembuf lock_res = {0, -1, 0}; // блокировка ресурса
    struct sembuf kill_res = {1, -1, IPC_NOWAIT}; // декремент счетчика
    struct sembuf rel_res = {0, 1, 0}; // освобождение ресурса
    // Получим ключ, Один и тот же ключ можно использовать как для семафора, так и для разделяемой памяти
    if((key = ftok(".", 'S')) < 0) {
        printf("Невозможно получить ключ\n");
        exit(1);
    }
    semid = semget(key, 2, 0666 | IPC_CREAT); // Создадим семафор - для синхронизации работы с разделяемой памятью.
    arg.val = 1; // Установить в семафоре №0 (Контроллер ресурса) значение "1"
    arg2.val = space - 1; // Установить в семафоре №1 (Контроллер потомков) значение "space - 1"
    semctl(semid, 0, SETVAL, arg);
    semctl(semid, 1, SETVAL, arg2);
    // Создадим область разделяемой памяти
    if((shmid = shmget(key, sizeof(double), IPC_CREAT | 0666)) < 0) {
        perror("shmget");
        exit(1);
    }
    for(int i = 0; i < space; i++) {
        pid[i] = fork();
        srand(getpid());
        if (0 == pid[i]) {
            while(1) {
                printf("PID = %d, i = %d\n", getpid(), i);
                fflush(stdout);
                // Получим доступ к разделяемой памяти
                if((shm = (double *)shmat(shmid, NULL, 0)) == (double *) -1) {
                    perror("shmat");
                    exit(1);
                }
                printf("Процесс ожидает PID=%d i=%d\n", getpid(), i);
                fflush(stdout);
                // Заблокируем разделяемую память
                if((semop(semid, &lock_res, 1)) == -1) {
                    fprintf(stderr, "Блокировка потомком неудачна\n");
                    exit(1);
                } else {
                    printf("Декремент потомка (locked) i=%d\n", i);
                    fflush(stdout);
                }
                // Запишем в разделяемую память сумму
                *(shm) = getpid();
                fflush(stdout);
                sleep(rand()%1);
                // Освободим разделяемую память
                if((semop(semid, &rel_res, 1)) == -1) {
                    fprintf(stderr, "Разблокировка потомком неудачна\n");
                    exit(1);
                } else {
                    printf("Инкремент потомка (unlocked) i=%d\n", i);
                    fflush(stdout);
                }
                printf("Kamikaze = %d\n", (int)*shm);
                fflush(stdout);
                // Отключимся от разделяемой памяти
                if(shmdt(shm) < 0) {
                    perror("shmdt");
                    exit(1);
                }
                sleep(rand()%1);
            }
            exit(0);
        } else if(pid[i] < 0) {
            perror("fork"); // произошла ошибка
            exit(1); // выход из родительского процесса
        }
    }

    for (int i = 0; i < space; i++) {
        // Получим доступ к разделяемой памяти
        if((shm = (double*)shmat(shmid, NULL, 0)) == (double *) -1) {
            perror("shmat");
            exit(1);
        }
        // Заблокируем разделяемую память
        if((semop(semid, &lock_res, 1)) == -1) {
            fprintf(stderr, "Блокировка родителем неудачна\n");
            exit(1);
        } else {
            printf("Декремент родителя (locked)\n");
            fflush(stdout);
        }
        sleep(5);
        kill((int)*shm, SIGKILL);
        printf("Потомок %d был убит\n", (int)*shm);
        fflush(stdout);
        // Декремент количества процессов-потомков
        j = (semop(semid, &kill_res, 1));
        //printf("Semop Kill_Res = %d\n", j);
        if(j == -1) {
            fprintf(stderr, "Убийство потомка неудачно\n");
            exit(1);
        } else {
            printf("Kill one\n");
            fflush(stdout);
        }
        // Освободим разделяемую память
        if((semop(semid, &rel_res, 1)) == -1) {
            fprintf(stderr, "Разблокировка родителя неудачна\n");
            exit(1);
        } else {
            printf("Инкремент родителя (unlocked)\n");
            fflush(stdout);
        }
        // Отключимся от разделяемой памяти
        if (shmdt(shm) < 0) {
            perror("shmdt");
            exit(1);
        }
    }
    // ожидание окончания выполнения всех запущенных процессов
    for(int i = 0; i < space; i++) {
        wpid = waitpid(pid[i], &status, 0);
        if(pid[i] == wpid) {
            printf("Потомок %d завершен, result=%d\n", i, WEXITSTATUS(status));
            printf("Победитель %d!\n", (int)*shm);
            fflush(stdout);
        }
    }
    /* Удалим созданные объекты IPC */
    if (shmctl(shmid, IPC_RMID, 0) < 0) {
        printf("Невозможно удалить область\n");
        exit(1);
    } else
        printf("Сегмент памяти помечен для удаления\n");
    if(semctl(semid, 0, IPC_RMID) < 0) {
        printf("Невозможно удалить семафор\n");
        exit(1);
    }
}
int main(int argc, char **argv) {
    if(argc < 2) {
        printf("Исползовать: ./9lab childs\n");
        exit(-1);
    }
    HungerGames(argv[1]);
    return 0;
}