#include <stdio.h>      // for printf() and fprintf()
#include <sys/socket.h> // for socket() and bind()
#include <arpa/inet.h>  // for sockaddr_in and inet_ntoa()
#include <stdlib.h>     // for atoi() and exit()
#include <string.h>     // for memset()
#include <unistd.h>     // for close()
#include <mcheck.h>     // для mtrace

#define ECHOMAX 255     // Longest string to echo
#define MAX_LEN 10      // максимальная длина строки с учетом переноса

void DieWithError(char *errorMessage); // External error handling function

char** creatMas(int count) {
    char buffer[count];
    char **mas;
    mas = (char **)malloc(sizeof(char *)*count);
    for (int i = 0; i < count; i++) {
        mas[i] = (char *)malloc(sizeof(char) * MAX_LEN);
        for (int j = 0; j < count; j++) {
            buffer[j] = (rand() % 2) + '0'; // ЧУДО ЧУДЕСНОЕ!
        }
        strncpy(mas[i], buffer, MAX_LEN);
    }
    return mas;
}
void printMas(char **mas, int count) {
    for (int i = 0; i < count; i++) {
        printf("%s\n", mas[i]);
    }
}
void freeMas(char **mas, int count) {
    for (int i = 0; i < count; i++) {
        free(mas[i]); // память для отдельной строки
    }
    free(mas); // память для массива указателей на строки
}


int main(int argc, char *argv[]) {
    int sock[MAX_LEN];                           // Socket
    struct sockaddr_in echoServAddr[MAX_LEN];    // Local address
    struct sockaddr_in echoClntAddr;    // Client address
    unsigned int cliAddrLen;            // Length of incoming message
    char echoBuffer[ECHOMAX];           // Buffer for echo string
    unsigned short echoServPort[MAX_LEN];        // Server port
    int recvMsgSize;                    // Size of received message

    srand(getpid());
    int x, y, block[65535], dx, dy, ii;
    char coordinates;

    if (argc != 2) { // Test for correct number of parameters
        fprintf(stderr, "Usage:  %s <UDP SERVER PORT>\n", argv[0]);
        exit(1);
    }

    echoServPort[0] = atoi(argv[1]); // First arg:  local port
    for (int i = 0; i < MAX_LEN; i++) {
        if (i > 0) echoServPort[i] = echoServPort[i - 1] + 1;

        /* Create socket for sending/receiving datagrams */
        if ((sock[i] = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
            DieWithError("socket() failed");

        /* Construct local address structure */
        memset(&echoServAddr[i], 0, sizeof(echoServAddr[i]));   // Zero out structure
        echoServAddr[i].sin_family = AF_INET;              // Internet address family
        echoServAddr[i].sin_addr.s_addr = htonl(INADDR_ANY); // Any incoming interface
        echoServAddr[i].sin_port = htons(echoServPort[i]);      // Local port

        /* Bind to the local address */
        if (bind(sock[i], (struct sockaddr *) &echoServAddr[i], sizeof(echoServAddr[i])) < 0)
            DieWithError("bind() failed");

    }

    /* Matrix */
    mtrace();
    char **mas = NULL;
    mas = creatMas(MAX_LEN);
    printMas(mas, MAX_LEN);

    for (int i = 0; i < MAX_LEN; i++) {
        block[i] = 0;
    }
    dx = 0;
    dy = 0;
    ii = 0;

    while (1) {
        /* Set the size of the in-out parameter */
        cliAddrLen = sizeof(echoClntAddr);

        /* Block until receive message from a client */
        if ((recvMsgSize = recvfrom(sock[0], echoBuffer, ECHOMAX, 0,
                                    (struct sockaddr *) &echoClntAddr, &cliAddrLen)) < 0)
            DieWithError("recvfrom() failed");

        printf("Handling client %s\n", inet_ntoa(echoClntAddr.sin_addr));
        printf("Handling client %d\n", echoClntAddr.sin_port);
        printf("echoBuffer = %s\n", echoBuffer);
        x = atoi(echoBuffer) / 10;
        y = atoi(echoBuffer) % 10;
        coordinates = mas[y][x];

        if (block[echoClntAddr.sin_port] == 0) {
            if (x > MAX_LEN / 2) dx = -1; else dx = 1;
            if (y > MAX_LEN / 2) dy = -1; else dy = 1;
            if (x > y) dy = 0; else dx = 0;
            block[echoClntAddr.sin_port] = 1;
        }
        echoBuffer[0] = (x + dx) + '0';
        echoBuffer[1] = (y + dy) + '0';
        if ((x == 0) || (x == 9) || (y == 0) || (y == 9)) block[echoClntAddr.sin_port] = 0;

        if (mas[y][x] == '1') coordinates = '0';
        mas[y][x] = '*';
        printMas(mas, MAX_LEN);
        mas[y][x] = coordinates;

        /* Send received datagram back to the client */
        if (sendto(sock[0], echoBuffer, recvMsgSize, 0,
                   (struct sockaddr *) &echoClntAddr, sizeof(echoClntAddr)) != recvMsgSize)
            DieWithError("sendto() sent a different number of bytes than expected");
    }

    freeMas(mas, MAX_LEN);
    /* NOT REACHED */
}