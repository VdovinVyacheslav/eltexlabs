#include <pthread.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <signal.h>

#define MAXNITEMS 100
#define MAXNTHREADS 10
#define min(a,b) ((a) < (b) ? (a) : (b))

static int nitems;
struct {
    pthread_mutex_t mutex; // инициализирует взаимоисключающую блокировку
    long int buff[MAXNITEMS]; // массив символов
    int nput; // номер элемента
    long int nval; // значение элемента
    int j; // для блокировки создания новых потоков
} shared = PTHREAD_MUTEX_INITIALIZER; // взаимные исключения по стандарту Posix, переменная

void *produce(void *arg) {
    while (1) {
        pthread_mutex_lock(&shared.mutex);
        if (shared.nput >= nitems) {
            pthread_mutex_unlock(&shared.mutex);
            return (NULL);  // массив заполнен
        }
        shared.nval = pthread_self();
        shared.buff[shared.nput] = shared.nval; // 4Е 3А
        if (shared.j == 0) {
            printf("Producer #%lu finished element %d\n", pthread_self(), shared.nput);
            *((int *) arg) += 1; // count++
        }
        shared.nput++;
        pthread_mutex_unlock(&shared.mutex);
        sleep(rand() % 1);
    }
}

void *consume(void *arg) {
    int i;
    for (i = 0; i < nitems; i++) {
        while (1) {
            pthread_mutex_lock(&shared.mutex);
            if (i < shared.nput) {
                pthread_mutex_unlock(&shared.mutex);
                break; // элемент готов
            } else {
                printf("Attention! Element %d not done by producer! Welcome to GULAG!\n", i);
                shared.j++; // инкремент блокировщика
                pthread_mutex_unlock(&shared.mutex);
                break;
            }
        }
    }
    return (NULL);
}

int main(int argc, char **argv) {
    int i, nthreads, count[MAXNTHREADS];
    pthread_t tid_consume, tid_produce[MAXNTHREADS];

    if (argc != 3) {
        printf("./10lab <#items> <#threads>, is recommended <20> <5>");
        exit(-1);
    }
    shared.j = 0; // сброс блокировщика
    nitems = min(atoi(argv[1]), MAXNITEMS); // кол-во элементов
    nthreads = min(atoi(argv[2]), MAXNTHREADS); // кол-во потоков

    for (i = 0; i < nthreads; i++) {
        count[i] = 0; // обнуление значения, возвращаемого функцией потока
        pthread_create(&tid_produce[i], NULL, produce, &count[i]); // создает поток для выполнения функции производителя
        sleep(rand() % 1);
    }
    pthread_create(&tid_consume, NULL, consume, NULL); // создает поток для выполнения функции потребителя
    sleep(1);
    printf("\n");
    for (i = 0; i < nthreads; i++) {
        pthread_join(tid_produce[i], NULL); // ожидает завершение неотсоединенного процессапроизводителя
        printf("Producer: #%lu made %d elements\n", tid_produce[i], count[i]);
    }
    pthread_join(tid_consume, NULL); // ожидает завершение неотсоединенного процесса потребителя
    if (shared.j == 0) printf("Consumer: Mission complete, Tovarisch!\n");
    exit(0);
}