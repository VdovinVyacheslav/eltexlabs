#! /bin/bash
while [[ "$alarm" != "x" ]]; do
	echo 'Alarm. Select an action:'
	echo '(c)reate, (d)elete, (e)dit, (k)ill, c(l)ear, (s)how, e(x)it:'
	read alarm
	case $alarm in
		c)	echo 'Select hour(hh):'
			read h
			echo 'Select minutes(mm):'
			read m
			echo 'Choose a ringtone(1-3):'
			read melody
			# пути к музыке, заработал только mpg123, будешь слушать блюз
			case $melody in
				1)	way='/home/it/Documents/eltexlabs/3Lab/LonesideMyBed.mp3';;
				2)	way='/home/it/Documents/eltexlabs/3Lab/MyName.mp3';;
				3)	way='/home/it/Documents/eltexlabs/3Lab/WayDown.mp3';;
			esac
			echo -e "$m $h * * *  mpg123 $way\n" >> /tmp/cron.txt
			crontab /tmp/cron.txt
			echo -e "$m $h $way" >> /tmp/buffer.txt
			cat -n /tmp/buffer.txt > /tmp/stupidcron.txt;;

		d)	echo 'Enter the alarm number(1, 2,...):'
			read N
			#в cron команды в нечетных строках, нумерация, по-человечески, с 1ой
			let "M = N"
			let "N = N-1"
			let "N = N*2+1"
			#колхоз с sed, но работает
			sed -i "${N}d" /tmp/cron.txt
			sed -i "${N}d" /tmp/cron.txt
			crontab /tmp/cron.txt
			sed -i "${M}d" /tmp/buffer.txt
			cat -n /tmp/buffer.txt > /tmp/stupidcron.txt;;

		e)	#изменить=удалить+создать, т.е. удаляет указанную строку и новую добавляет в конец
			cat /tmp/stupidcron.txt
			echo 'Enter the alarm number(1, 2,...):'
			read N
			let "M = N"
			let "N = N-1"
			let "N = N*2+1"
			sed -i "${N}d" /tmp/cron.txt
			sed -i "${N}d" /tmp/cron.txt
			crontab /tmp/cron.txt
			sed -i "${M}d" /tmp/buffer.txt
			cat -n /tmp/buffer.txt > /tmp/stupidcron.txt

			echo 'Select hour(hh):'
			read h
			echo 'Select minutes(mm):'
			read m
			echo 'Choose a ringtone(1-3):'
			read melody
			case $melody in
				1)	way='/home/it/Documents/eltexlabs/3Lab/LonesideMyBed.mp3';;
				2)	way='/home/it/Documents/eltexlabs/3Lab/MyName.mp3';;
				3)	way='/home/it/Documents/eltexlabs/3Lab/WayDown.mp3';;
			esac
			echo -e "$m $h * * *  mpg123 $way\n" >> /tmp/cron.txt
			crontab /tmp/cron.txt
			echo -e "$m $h $way" >> /tmp/buffer.txt
			cat -n /tmp/buffer.txt > /tmp/stupidcron.txt;;

		k)	pkill mpg123;;

		l)	cat /dev/null > /tmp/cron.txt
			cat /dev/null > /tmp/stupidcron.txt
			cat /dev/null > /tmp/buffer.txt
			crontab -r;;

		s)	cat /tmp/stupidcron.txt;;

		x)	break;;
		*)	echo '...';;
	esac
done
exit 0