//Варианты:1
//Фамилия 
//Год рождения 
//Номер отдела 
//Оклад 
//Расположить записи в массиве в порядке возрастания (по году рождения)

#include <stdio.h>
#include <stdlib.h>
#include <mcheck.h> //mtrace
#define YEAR 1918

struct worker{
    char surname[50];
    int year;
    int depart;
    int salary;
};
typedef struct worker workers;
 
void readworker(workers *st){
    printf("Введите фамилию:");
    scanf("%s", st->surname);
    printf("Введите год рождения:");
    scanf("%d", &st->year);
    printf("Введите номер департамента:");
    scanf("%d", &st->depart);
    printf("Введите ЗП:");
    scanf("%d", &st->salary);
}

static int cmp(const void *p1, const void *p2){
    workers * st1 = *(workers**)p1;
    workers * st2 = *(workers**)p2;
    return st2->year - st1->year;
}

int main(int argc, char **argv){
    int count = 0;
    mtrace();	//утечка памяти
    printf("Введите кол-во работников:");
    scanf("%d", &count);
    workers** st = (workers**)malloc(sizeof(workers*)*count);
    for (int i = 0; i < count ; i++){
        st[i] = (workers*) malloc (sizeof(workers));
        readworker(st[i]);
    } 
    qsort(st, count, sizeof(workers*), cmp);

// захерачить в функции
    printf("\n");
    for (int i = 0; i < count; i++){
    printf("Работник %d: %s\n", i, st[i]->surname);
    printf("Год рождения:%d\n", st[i]->year);
    }
    
    for (int i = 0; i < count; i++){
    free(st[i]);
    }
    free(st);
    return 0;
}
