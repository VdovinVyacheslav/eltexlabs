//1. Поиск указанной строки в указанном файле. Обработка одной строки в порожденном процессе.

#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#define N 1024

int main(int argc, char *argv[]) {
    int i, pid[argc], status, stat;
    FILE *fp[argc];
    char ch;
    // для всех файлов, перечисленных в командной строке
    if (argc < 4) {
        printf("Usage: file string outfile infile1 ...\n");
        exit(-1);
    }
    if ((fp[2] = fopen (argv[2], "a")) == NULL) {
        printf ("Невозможно открыть записываемый файл.\n");
        exit (-1);
    }
    for (i = 3; i < argc; i++) {
        // запускаем дочерний процесс
        pid[i] = fork();
        if (pid[i] == 0) {
            // если выполняется дочерний процесс
//========================================================================
            printf( "processing of file %s started (pid=%d)\n", argv[i],pid[i]);
            if ((fp[i] = fopen (argv[i], "r")) == NULL) {
                printf ("Невозможно открыть читаемый файл.\n");
                exit (-2);
            }
            char buffer[N];
            int len = 0;
            while ((ch = fgetc (fp[i])) != EOF) {
                if (ch == '\n') {
                    if (!strcmp(buffer, argv[1])) {
                        for (int j = 0; j < len; j++) {
                            fputc (buffer[j], fp[2]);
                        }
                    }
                    len = 0;
                }
                buffer[len] = ch;
                //printf ("%c", ch);
                len++;
            }
            fclose (fp[i]);
//========================================================================
        } else if (-1 == pid[i]) {
            perror("fork"); /* произошла ошибка */
            exit(1); /*выход из родительского процесса*/
        }
        // если выполняется родительский процесс
    }
    sleep(5);
    // ожидание окончания выполнения всех запущенных процессов
    for (i = 3; i < argc; i++) {
        status = waitpid(pid[i],&stat,WNOHANG);
        if (pid[i] == status) {
            printf("File %s done, result=%d\n",argv[i],WEXITSTATUS(stat));
            exit(-2);
        }
    }
    fclose (fp[2]);
    return 0;
}