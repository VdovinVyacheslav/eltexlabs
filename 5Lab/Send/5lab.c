/*1.Разработать функции для выполнения арифметических операций по вариантам. 
2.Оформить статическую библиотеку функций и написать программу, ее использующую.
3.Переоформить библиотеку, как динамическую, но подгружать статически, при компиляции.
4.Изменить программу для динамической загрузки функций из библиотеки.
Операции сложения и вычитания.

gcc -fPIC -c lib.c
gcc -shared lib.o -o libpowers.so
gcc 5lab.c -o Send -ldl
*/

#include <stdio.h>
#include <dlfcn.h>
extern int f1();
extern int f2();

int main(int argc, char* argv[]){
/*
int n1, n2, sum, sub;
n1 = f1();n2 = f2();
sum = n1 + n2; sub = n1 - n2;
printf("%d+%d=%d\n",n1, n2, sum);
printf("%d-%d=%d\n",n1, n2, sub);
*/

void *ext_library;
int value1=0, value2=0;
int (*powerfunc)(int x, int y);
ext_library = dlopen ("/home/icp/eltexlabs/Lab5/Send/libpowers.so",RTLD_LAZY);
if (!ext_library){
	fprintf(stderr,"dlopen() error: %s\n", dlerror());
	return 1;
};
powerfunc = dlsym(ext_library, argv[1]);
if (!powerfunc){
	fprintf(stderr,"dlopen() error: %s\n", dlerror());
	return 1;
};
value1 = 36; value2 = 15;
printf("%d power %d = %d", value1, value2, (*powerfunc)(value1, value2));
dlclose(ext_library);

return 0;
}
