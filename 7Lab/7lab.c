/* 5. Винни-Пух и пчелы. Заданное количество пчел добывают мед равными порциями, задерживаясь в пути на случайное время. Винни-Пух потребляет мед порциями заданной величины за заданное время и столько же времени может прожить без питания. Работа каждой пчелы реализуется в порожденном процессе. */

#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/wait.h>
#define FIFO1 "/tmp/fifo.1"
#define MAXLINE 80

int volume = 1000;

void bear(char **argv) {
    pid_t pid;
    int status = 0;
    int readfd, writefd;
    char buff[MAXLINE+1];
    pid = fork();
    mkfifo(FIFO1, 0666);
    if (-1 == pid) {
        perror("fork"); // произошла ошибка
        exit(1); // выход из родительского процесса
    } else if (0 == pid) {
        printf("Это медведь\n");
        writefd = open(FIFO1, O_WRONLY, 0);
        sleep(1);
        write(writefd, argv[3], sizeof(argv[3])); // запись в канал IPC
        close(writefd);
        exit(status); // выход из процесс-потомока
    } else {
        printf("Медведь на связи!\n");
        readfd = open(FIFO1, O_RDONLY, 0);
        read(readfd, buff, sizeof(buff)); // считывание из канала
        close(readfd);
        volume -= atoi(buff);
        printf("Запас после медведя =%d\n", volume);
        if (wait(&status) == -1) {
            perror("wait() error");
        } else if (WIFEXITED(status)) {
            printf("Медведь: Код возврата потомка: %d\n", WEXITSTATUS(status));
        } else {
            perror("PARENT: потомок не завершился успешно");
        }
    }
}


int main(int argc, char *argv[]) {
    int i, pid[argc], status, stat, num_bee;
    num_bee = atoi(argv[1]);
    if (argc < 3) {
        printf("Введите: ./программа кол-во_пчел силу_пчелы силу_медведя\n");
        exit(-1);
    }
    int fd[num_bee][2]; // неименованные каналы ввод вывод
    while(volume > 100) {
        for (i = 0; i < num_bee; i++) {
            pipe(fd[i]);
            pid[i] = fork();
            srand(getpid());
            if (-1 == pid[i]) {
                perror("fork"); // произошла ошибка
                exit(1); // выход из родительского процесса
            } else if (0 == pid[i]) {
                close(fd[i][0]); // процесс-потомок закрывает доступный для чтения конец канала 0
                // записывает в канал 1
                int wr_bee = atoi(argv[2]);
                sleep(rand()%5);
                write(fd[i][1], &wr_bee, sizeof(int)); // пчела приносит мед в улей
                exit(0);
            }
        }
        // если выполняется родительский процесс
        printf("Улей на связи!\n");
        // ожидание окончания выполнения всех запущенных процессов
        for (i = 0; i < num_bee; i++) {
            status = waitpid(pid[i], &stat, 0);
            if (pid[i] == status) {
                printf("Пчела %d принесла мед, Код возврата потомка=%d\n", i, WEXITSTATUS(stat));
                close(fd[i][1]); // процесс-родитель закрывает доступный для записи конец канала 1
                // читает из канала 0
                int r_bee;
                read(fd[i][0], &r_bee, sizeof(int)); // улей пополняется
                volume += r_bee;
                printf("Запас после пчелы =%d\n", volume);
                if (volume < 100) break;
                bear(argv);
            }
        }
    }
    return 0;
}