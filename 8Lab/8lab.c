/* 4. Warcraft. Заданное количество юнитов добывают золото  порциями из одной шахты,
задерживаясь в пути на случайное время, до ее истощения. Работа каждого юнита реализуется в порожденном процессе. */

#include <sys/types.h>
#include <sys/stat.h>
#include <wait.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/errno.h>
#define MINE 1000

struct msgbuf {
    long mtype;
    int mine;
    int volume;
};

int msqid, rc;
size_t length;

void child_send_mess(int qid, struct msgbuf *qbuf, long type, int pargv) {
    qbuf->mtype = type;
    qbuf->mine = pargv;
    length = sizeof(struct msgbuf) - sizeof(long);
    if ((msgsnd(qid, qbuf, length, 0)) == -1) {
        perror("msgsnd");
        exit(1);
    }
}
void par_read_mess(int qid, struct msgbuf *qbuf, long type) {
    qbuf->mtype = type;
    length = sizeof(struct msgbuf) - sizeof(long);
    msgrcv(qid, qbuf, length, type, 0);
}
void par_send_mess(int qid, struct msgbuf *qbuf, long type, int vol) {
    qbuf->mtype = type;
    qbuf->volume = vol;
    length = sizeof(struct msgbuf) - sizeof(long);
    if ((msgsnd(qid, qbuf, length, 0)) == -1) {
        perror("msgsnd");
        exit(1);
    }
}
void child_read_mess(int qid, struct msgbuf *qbuf, long type) {
    qbuf->mtype = type;
    length = sizeof(struct msgbuf) - sizeof(long);
    msgrcv(qid, qbuf, length, type, 0);
}

int main(int argc, char *argv[]) {
    int i, pid[argc], status, stat;
    struct msgbuf qbuf;
    qbuf.volume = MINE;
    if (argc < 2) {
        printf("Используйте: ./программа сила_майнера1 сила_майнера2...\n");
        exit(-1);
    }
    if ((msqid = msgget(IPC_PRIVATE, 0666 | IPC_CREAT)) == -1) {
        perror("msgget");
        exit(1);
    }
    for (i = 1; i < argc; i++) {
        pid[i] = fork(); // запускаем дочерний процесс
        srand(getpid());
        if (pid[i] < 0) {
            perror("fork"); // произошла ошибка
            exit(1); // выход из родительского процесса
        } else if (0 == pid[i]) {
            sleep(i);
            while (qbuf.volume > 0) {
                child_read_mess(msqid, &qbuf, 2); // потомок читает из очереди
                child_send_mess(msqid, &qbuf, 1, atoi(argv[i])); // потомок пишет в очередь
                printf("Процесс-потомок %d майнит! PIDD = %d\n", i, getpid());
                fflush(stdout);
                sleep(i);
            }
            exit(i);
        }
    }

    // если выполняется родительский процесс
    while (qbuf.volume > 0) {
        par_send_mess(msqid, &qbuf, 2, qbuf.volume); // родитель пишет в очередь
        par_read_mess(msqid, &qbuf, 1); // потомок читает из очереди
        qbuf.volume -= qbuf.mine;
        if (qbuf.volume >= 0) printf("Запас золота = %d\n", qbuf.volume);
        fflush(stdout);
    }
    printf("Золотой рудник обрушился!\n");
    // ожидание окончания выполнения всех запущенных процессов
    sleep(5);
    for (i = 0; i < argc; i++) {
        status = waitpid(pid[i], &stat, WNOHANG);
        if (pid[i] == status) {
            printf("Процесс-потомок %d добыл, result=%d\n", i, WEXITSTATUS(stat));
            exit(-1);
        } else kill(pid[i], SIGKILL); // тут хз
    }
    if ((rc = msgctl(msqid, IPC_RMID, NULL)) < 0) {
        perror(strerror(errno));
        return 1;
    }
    return 0;
}