#include <stdio.h>      /* for printf() and fprintf() */
#include <sys/socket.h> /* for socket(), connect(), sendto(), and recvfrom() */
#include <arpa/inet.h>  /* for sockaddr_in and inet_addr() */
#include <stdlib.h>     /* for atoi() and exit() */
#include <string.h>     /* for memset() */
#include <unistd.h>     /* for close() */
#include <wait.h>       /* for sleep() */

#define ECHOMAX 255     /* Longest string to echo */
#define MAXPENDING 5    /* Maximum outstanding connection requests */
#define RCVBUFSIZE 32   /* Size of receive buffer */

void DieWithError(char *errorMessage);  /* External error handling function */

int main(int argc, char *argv[]) {

    int i, j, pid[4], status, stat;

    //UDP---------------------------------UDP
    int sock;                        /* Socket */
    struct sockaddr_in echoServAddr; /* Local address */
    struct sockaddr_in echoClntAddr; /* Client address */
    unsigned int cliAddrLen;         /* Length of incoming message */
    char echoBuffer[ECHOMAX];        /* Buffer for echo string */
    unsigned short echoServPort;     /* Server port */
    int recvMsgSize;                 /* Size of received message */

    if ((argc < 3) || (argc > 4)) {  /* Test for correct number of arguments */
        fprintf(stderr, "Usage: %s <Server IP> <Echo Word> [<Echo Port>]\n", argv[0]);
        exit(1);
    }

    if (argc == 4)
        echoServPort = atoi(argv[3]);  /* First arg:  local port */

    /* Create socket for sending/receiving datagrams */
    if ((sock = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
        DieWithError("socket() failed");

    /* Construct local address structure */
    memset(&echoServAddr, 0, sizeof(echoServAddr));   /* Zero out structure */
    echoServAddr.sin_family = AF_INET;                /* Internet address family */
    echoServAddr.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
    echoServAddr.sin_port = htons(echoServPort);      /* Local port */

    /* Bind to the local address */
    if (bind(sock, (struct sockaddr *) &echoServAddr, sizeof(echoServAddr)) < 0)
        DieWithError("bind() failed");

    //TCP---------------------------------TCP
    int sock1;                        /* Socket descriptor */
    struct sockaddr_in echoServAddr1; /* Echo server address */
    unsigned short echoServPort1;     /* Echo server port */
    char *servIP;                    /* Server IP address (dotted quad) */
    char *echoString1;                /* String to send to echo server */
    char echoBuffer1[RCVBUFSIZE];     /* Buffer for echo string */
    unsigned int echoStringLen1;      /* Length of string to echo */
    int bytesRcvd, totalBytesRcvd;   /* Bytes read in single recv()
                                        and total bytes read */

    servIP = argv[1];             /* First arg: server IP address (dotted quad) */
    echoString1 = argv[2];         /* Second arg: string to echo */
    echoServPort1 = atoi(argv[3]) + 256; /* Use given port, if any */


    for (j = 0; j < 2; j++) {
        // запускаем дочерний процесс
        pid[j] = fork();
        srand(getpid());
        if (pid[j] == 0) {
            // если выполняется дочерний процесс
            printf( "processing started (pid=%d)\n", getpid());

            if (j == 0) {
                while (1) {
                    //UDP---------------------------------UDP
                    /* Set the size of the in-out parameter */
                    cliAddrLen = sizeof(echoClntAddr);

                    /* Block until receive message from a client */
                    if ((recvMsgSize = recvfrom(sock, echoBuffer, ECHOMAX, 0,
                                                (struct sockaddr *) &echoClntAddr, &cliAddrLen)) < 0)
                        DieWithError("recvfrom() failed");

                    printf("Handling client %s\n", inet_ntoa(echoClntAddr.sin_addr));

                    /* Send received datagram back to the client */
                    if (sendto(sock, echoBuffer, recvMsgSize, 0,
                               (struct sockaddr *) &echoClntAddr, sizeof(echoClntAddr)) != recvMsgSize)
                        DieWithError("sendto() sent a different number of bytes than expected");
                }
            }

            if (j == 1) {
                sleep(5);
                while (1) {
                    //TCP---------------------------------TCP
                    /* Create a reliable, stream socket using TCP */
                    if ((sock1 = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
                        DieWithError("socket() failed");

                    /* Construct the server address structure */
                    memset(&echoServAddr1, 0, sizeof(echoServAddr1));     /* Zero out structure */
                    echoServAddr1.sin_family      = AF_INET;             /* Internet address family */
                    echoServAddr1.sin_addr.s_addr = inet_addr(servIP);   /* Server IP address */
                    echoServAddr1.sin_port        = htons(echoServPort1); /* Server port */

                    /* Establish the connection to the echo server */
                    if (connect(sock1, (struct sockaddr *) &echoServAddr1, sizeof(echoServAddr1)) < 0)
                        DieWithError("connect() failed");

                    echoStringLen1 = strlen(echoString1);          /* Determine input length */


                    /* Send the string to the server */
                    if (send(sock1, echoString1, echoStringLen1, 0) != echoStringLen1)
                        DieWithError("send() sent a different number of bytes than expected");

                    /* Receive the same string back from the server */
                    totalBytesRcvd = 0;
                    printf("Received: ");                /* Setup to print the echoed string */
                    while (totalBytesRcvd < echoStringLen1) {
                        /* Receive up to the buffer size (minus 1 to leave space for
                           a null terminator) bytes from the sender */
                        if ((bytesRcvd = recv(sock1, echoBuffer1, RCVBUFSIZE - 1, 0)) <= 0)
                            DieWithError("recv() failed or connection closed prematurely");
                        totalBytesRcvd += bytesRcvd;   /* Keep tally of total bytes */
                        echoBuffer1[bytesRcvd] = '\0';  /* Terminate the string! */
                        printf("%s", echoBuffer1);      /* Print the echo buffer */
                    }
                    printf("\n");    /* Print a final linefeed */
                    close(sock1);
                    //exit(0);
                }
            }
            //========================================================================
        } else if (-1 == pid[j]) {
            perror("fork"); /* произошла ошибка */
            exit(1); /*выход из родительского процесса*/
        }
        // если выполняется родительский процесс
    }

    sleep(30);
    // ожидание окончания выполнения всех запущенных процессов
    for (i = 0; i < 4; i++) {
        status = waitpid(pid[i], &stat, WNOHANG);
        if (pid[i] == status) {
            printf("Process done, result=%d\n", WEXITSTATUS(stat));
            exit(-2);
        }
    }

}