#include <stdio.h>      // for printf() and fprintf()
#include <sys/socket.h> // for socket() and bind()
#include <arpa/inet.h>  // for sockaddr_in and inet_ntoa()
#include <stdlib.h>     // for atoi() and exit()
#include <string.h>     // for memset()
#include <unistd.h>     // for close()
#include <wait.h>       // for sleep()

#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <sys/ipc.h>
#include <sys/msg.h>
#include <sys/errno.h>
#define MINE 1000
/*
struct msgbuf {
    long mtype;
    int mine;
};

int msqid, rc;
size_t length;

void child_send_mess(int qid, struct msgbuf *qbuf, long type, int pargv) {
    qbuf->mtype = type;
    qbuf->mine = pargv;
    length = sizeof(struct msgbuf) - sizeof(long);
    if ((msgsnd(qid, qbuf, length, 0)) == -1) {
        perror("msgsnd");
        exit(1);
    }
}
void par_read_mess(int qid, struct msgbuf *qbuf, long type) {
    qbuf->mtype = type;
    length = sizeof(struct msgbuf) - sizeof(long);
    msgrcv(qid, qbuf, length, type, 0);
}
void par_send_mess(int qid, struct msgbuf *qbuf, long type) {
    qbuf->mtype = type;
    length = sizeof(struct msgbuf) - sizeof(long);
    if ((msgsnd(qid, qbuf, length, 0)) == -1) {
        perror("msgsnd");
        exit(1);
    }
}
void child_read_mess(int qid, struct msgbuf *qbuf, long type) {
    qbuf->mtype = type;
    length = sizeof(struct msgbuf) - sizeof(long);
    msgrcv(qid, qbuf, length, type, 0);
}
*/

#define MAXPENDING 5    /* Maximum outstanding connection requests */
#define ECHOMAX 255     /* Longest string to echo */
#define RCVBUFSIZE 32   /* Size of receive buffer */
#define COUNTSOCKET 32

void DieWithError(char *errorMessage);  /* External error handling function */
void HandleClient(int clntSocket) {
    char echoBuffer[RCVBUFSIZE];        /* Buffer for echo string */
    int recvMsgSize;                    /* Size of received message */
    /* Receive message from client */
    if ((recvMsgSize = recv(clntSocket, echoBuffer, RCVBUFSIZE, 0)) < 0)
        DieWithError("recv() failed");
    /* Send received string and receive again until end of transmission */
    while (recvMsgSize > 0) {     /* zero indicates end of transmission */

        /* Echo message back to client */
        if (send(clntSocket, echoBuffer, recvMsgSize, 0) != recvMsgSize)
            DieWithError("send() failed");
        /* See if there is more data to receive */
        if ((recvMsgSize = recv(clntSocket, echoBuffer, RCVBUFSIZE, 0)) < 0)
            DieWithError("recv() failed");
    }
    close(clntSocket);    /* Close client socket */
}

int main(int argc, char *argv[]) {

    int i, j, pid[4], status, stat;

    //TCP---------------------------------TCP
    int servSock[COUNTSOCKET];                    /* Socket descriptor for server */
    int clntSock[COUNTSOCKET];                    /* Socket descriptor for client */
    struct sockaddr_in echoServAddr1;               /* Local address */
    struct sockaddr_in echoClntAddr1;               /* Client address */
    unsigned short echoServPort1[COUNTSOCKET];     /* Server port */
    unsigned int clntLen[COUNTSOCKET];            /* Length of client address data structure */

    echoServPort1[0] = atoi(argv[3]) + 256;  /* First arg:  local port */

    for (i = 0; i < COUNTSOCKET; i++) {
        if (i > 0) echoServPort1[i] = echoServPort1[i - 1] + 1;

        /* Create socket for incoming connections */
        if ((servSock[i] = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0)
            DieWithError("socket() failed");

        /* Construct local address structure */
        memset(&echoServAddr1, 0, sizeof(echoServAddr1));   /* Zero out structure */
        echoServAddr1.sin_family = AF_INET;                /* Internet address family */
        echoServAddr1.sin_addr.s_addr = htonl(INADDR_ANY); /* Any incoming interface */
        echoServAddr1.sin_port = htons(echoServPort1[i]);      /* Local port */

        /* Bind to the local address */
        if (bind(servSock[i], (struct sockaddr *) &echoServAddr1, sizeof(echoServAddr1)) < 0)
            DieWithError("bind() failed");

        /* Mark the socket so it will listen for incoming connections */
        if (listen(servSock[i], MAXPENDING) < 0)
            DieWithError("listen() failed");
    }

    //UDP---------------------------------UDP
    int sock[COUNTSOCKET];                        /* Socket descriptor */
    struct sockaddr_in echoServAddr[COUNTSOCKET]; /* Echo server address */
    struct sockaddr_in fromAddr;     /* Source address of echo */
    unsigned short echoServPort[COUNTSOCKET];     /* Echo server port */
    unsigned int fromSize;           /* In-out of address size for recvfrom() */
    char *servIP;                    /* IP address of server */
    char *echoString;                /* String to send to echo server */
    char echoBuffer[ECHOMAX + 1];    /* Buffer for receiving echoed string */
    int echoStringLen;               /* Length of string to echo */
    int respStringLen;               /* Length of received response */

    if ((argc < 3) || (argc > 4)) {  /* Test for correct number of arguments */
        fprintf(stderr, "Usage: %s <Client IP> <Echo Word> [<Echo Port>]\n", argv[0]);
        exit(1);
    }

    servIP = argv[1];           /* First arg: server IP address (dotted quad) */
    echoString = argv[2];       /* Second arg: string to echo */
    echoServPort[0] = atoi(argv[3]);  /* Use given port, if any */

    for (i = 0; i < COUNTSOCKET; i++) {
        if ((echoStringLen = strlen(echoString)) > ECHOMAX)  /* Check input length */
            DieWithError("Echo word too long");

        if (i > 0) echoServPort[i] = echoServPort[i - 1] + 1;

        /* Create a datagram/UDP socket */
        if ((sock[i] = socket(PF_INET, SOCK_DGRAM, IPPROTO_UDP)) < 0)
            DieWithError("socket() failed");

        /* Construct the server address structure */
        memset(&echoServAddr[i], 0, sizeof(echoServAddr[i]));    /* Zero out structure */
        echoServAddr[i].sin_family = AF_INET;                 /* Internet addr family */
        echoServAddr[i].sin_addr.s_addr = inet_addr(servIP);  /* Server IP address */
        echoServAddr[i].sin_port   = htons(echoServPort[i]);     /* Server port */
    }

    for (j = 0; j < 2; j++) {
        // запускаем дочерний процесс
        pid[j] = fork();
        srand(getpid());
        if (pid[j] == 0) {
            // если выполняется дочерний процесс
            printf( "processing started (pid=%d)\n", getpid());

            if (j == 0) {
                sleep(2);
                while (1) {
                    //UDP---------------------------------UDP
                    for (i = 0; i < COUNTSOCKET; i++) {
                        /* Send the string to the server */
                        if (sendto(sock[i], echoString, echoStringLen, 0, (struct sockaddr *)
                                   &echoServAddr[i], sizeof(echoServAddr[i])) != echoStringLen)
                            DieWithError("sendto() sent a different number of bytes than expected");

                        /* Recv a response */
                        fromSize = sizeof(fromAddr);
                        if ((respStringLen = recvfrom(sock[i], echoBuffer, ECHOMAX, 0,
                                                      (struct sockaddr *) &fromAddr, &fromSize)) != echoStringLen)
                            DieWithError("recvfrom() failed");

                        if (echoServAddr[i].sin_addr.s_addr != fromAddr.sin_addr.s_addr) {
                            fprintf(stderr, "Error: received a packet from unknown source.\n");
                            exit(1);
                        }

                        /* null-terminate the received data */
                        echoBuffer[respStringLen] = '\0';
                        printf("Received: %s len=%d\n", echoBuffer, respStringLen);    /* Print the echoed arg */
                        close(sock[i]);
                    }
                }
            }

            if (j == 1) {
                while (1) {
                    //TCP---------------------------------TCP
                    for (i = 0; i < COUNTSOCKET; i++) {

                        // Set the size of the in-out parameter
                        clntLen[i] = sizeof(echoClntAddr1);

                        // Wait for a client to connect
                        if ((clntSock[i] = accept(servSock[i], (struct sockaddr *) &echoClntAddr1,
                                                  &clntLen[i])) < 0)
                            DieWithError("accept() failed");

                        // clntSock is connected to a client!
                        printf("Handling client %s\n", inet_ntoa(echoClntAddr1.sin_addr));
                        HandleClient(clntSock[i]);
                    }
                }
            }
            //========================================================================
        } else if (-1 == pid[j]) {
            perror("fork"); /* произошла ошибка */
            exit(1); /*выход из родительского процесса*/
        }
        // если выполняется родительский процесс
    }

    sleep(30);
    // ожидание окончания выполнения всех запущенных процессов
    for (i = 0; i < 4; i++) {
        status = waitpid(pid[i], &stat, WNOHANG);
        if (pid[i] == status) {
            printf("Process done, result=%d\n", WEXITSTATUS(stat));
            exit(-2);
        }
    }

}