/*1. Написать программу, обрабатывающую текстовый файл
и записывающую обработанные данные в файл (табл. 3).

Таблица 4.
Варианты
1
Удалить из текста заданный символ 
Параметры командной строки:
    1. Имя входного файла 
    2. Заданный символ
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

int main(int argc, char *argv[]){
if(argc < 3){
    printf("Неверное кол-во параметров\n");
    exit(1);
}
FILE *fp, *fp2;
char ch;
if((fp = fopen (argv[1], "r")) == NULL){
    printf("Невозможно открыть файл.\n");
    exit(1);
}
if((fp2 = fopen (argv[2], "w")) == NULL){
    printf("Невозможно открыть файл.\n");
    exit(1);
}

while((ch=fgetc(fp)) != EOF){
    if(ch != *(argv[3])){
    printf("%c", ch);
    }
}

fclose(fp2);
fclose(fp);
printf("\n");
return 0;
}
