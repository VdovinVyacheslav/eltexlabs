// Расположить строки по возрастанию длины

#include <stdio.h>
#include <stdlib.h> 			//malloc free
#include <mcheck.h> 			//mtrace
#include <string.h> 			//strlen strcpy
#define MAX_LEN 128				//предельная длина строки

char** readMas(int count){
	char buffer[MAX_LEN];
	char **mas;  				//указатель на массив указателей на строки
	mas = (char **)malloc(sizeof(char *)*count);//выделяем память для массива указателей
    for (int i = 0; i < count ; i++){
        scanf("%s", buffer);	//читаем строку в буфер
        mas[i] = (char *)malloc(sizeof(char)*strlen(buffer));//выделяем память для строки
        strcpy(mas[i], buffer);	//копируем строку из буфера в массив указателей
    }
    return mas;
}

void sortMas(char **mas, int count, int *amt){
	int gap, i, j;   
	char *temp;
	for (gap = count/2; gap > 0; gap /= 2){
		for (i = gap; i < count; i++){
			for (j = i - gap; j >= 0; j -= gap){ 
				if (strlen(mas[j]) <= strlen(mas[j+gap])) break;	//сравниваем длины
        		temp = mas[j];
        		mas[j] = mas[j+gap];
        		mas[j+gap] = temp;
        		*amt += 1;		//через указатель считаем перестановки
        	}
    	}
	}
}

void printMas(char **mas, int count, int *length){
    for (int i = 0; i < count ; i++){
        printf("%s\n", mas[i]); //выводим построчно
        if (strlen(mas[i]) < *length) *length = strlen(mas[i]);	//определяем длину
    }
}

void freeMas(char **mas, int count){
	for (int i = 0; i < count; i++){
        free(mas[i]); 			//освобождаем память строки
    }
    free(mas); 					//освобождаем памать массива указателей
}

int main(int argc, char **argv){
	char **mas = NULL;			//указатель на указатели
	int count, amt = 0, length = MAX_LEN;
	scanf("%d", &count);		//читаем кол-во строк (почему &???)
	mtrace();					//утечка памяти
	mas = readMas(count);		//читаем строки
	sortMas(mas, count, &amt);		//сортируем строки
	printMas(mas, count, &length);		//выводим строки, определяем длину
	printf("Выполнено перестановок %d\n", amt); //выводим кол-во перестановок
	printf("Длина строки %d\n", length);	//выводим длину строки
	freeMas(mas, count);		//освобождаем память
}